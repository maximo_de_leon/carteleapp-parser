package utils;

import com.google.gson.Gson;
import domain.CaribbeanCinemasMovieInfo;
import domain.Movie;
import domain.MovieFeatures;
import domain.PalacioDelCineMovieInfo;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * Created by maximodeleon on 11/11/16.
 */
public class MoviesMatcher {

    private Logger logger = Logger.getInstance(MoviesMatcher.class);
    private Map<String, Movie> moviesMap = new HashMap<String, Movie>();

    private Map<String, List<CaribbeanCinemasMovieInfo>> caribbeanMovies;
    private Map<String, List<PalacioDelCineMovieInfo>> palacioMovies;

    List<String> unWantedStrings = new ArrayList<String>();

    public MoviesMatcher(List<PalacioDelCineMovieInfo>  palacio, List<CaribbeanCinemasMovieInfo> caribbean) {

        unWantedStrings.add("(2D)");
        unWantedStrings.add("(Pel.Dom)");
        unWantedStrings.add("(3D)");
        unWantedStrings.add("(IMAX-3D)");
        unWantedStrings.add("(ESP)");
        unWantedStrings.add("(Sub-CXC)");
        unWantedStrings.add("(Sub-CXC-3D)");
        unWantedStrings.add("(Sub-4DX)");
        unWantedStrings.add("(Sub-3D)");
        unWantedStrings.add("(4DX)");
        unWantedStrings.add("(Esp-3D)");
        unWantedStrings.add("(ESP)");
        unWantedStrings.add("-");

        initializePalacioMap (palacio);
        initializeCaribbeanMap(caribbean);
    }


    private void initializePalacioMap(List<PalacioDelCineMovieInfo>  palacio) {
        palacioMovies = new HashMap<String, List<PalacioDelCineMovieInfo>>();
        for (PalacioDelCineMovieInfo mov : palacio) {
            String name = removeUnwantedChars(mov.getEnglishTitle());

            List<PalacioDelCineMovieInfo> list = palacioMovies.get(name);

            if (list == null) {
                list = new ArrayList<PalacioDelCineMovieInfo>();
                list.add(mov);
                palacioMovies.put(name, list);
            } else {
                list.add(mov);
            }
        }
    }

    private void initializeCaribbeanMap(List<CaribbeanCinemasMovieInfo> caribbean){

        caribbeanMovies = new HashMap<String, List<CaribbeanCinemasMovieInfo>>();

        for (CaribbeanCinemasMovieInfo mov : caribbean) {
            String name = removeUnwantedChars(mov.getName());

            List<CaribbeanCinemasMovieInfo> list = caribbeanMovies.get(name);
            if (list == null) {
                list = new ArrayList<CaribbeanCinemasMovieInfo>();
                list.add(mov);
                caribbeanMovies.put(name, list);
            } else {
                list.add(mov);
            }
        }

    }

    public void match() {
        Set<String> keys = new HashSet<String>();
        keys.addAll(caribbeanMovies.keySet());
        keys.addAll(palacioMovies.keySet());

        for (String key : keys) {
            addCaribbean(caribbeanMovies.get(key), key);
            addPalacio(palacioMovies.get(key), key);
        }
    }

    private void addCaribbean(List<CaribbeanCinemasMovieInfo> movies, String key) {
        if (movies != null) {
            for (CaribbeanCinemasMovieInfo mov : movies) {
                putInMap(mov, key);
            }
        }
    }

    public void addPalacio(List<PalacioDelCineMovieInfo> movies, String key) {
        if (movies != null) {
            for (PalacioDelCineMovieInfo mov : movies) {
                putInMap(mov, key);
            }
        }
    }

    private void putInMap(CaribbeanCinemasMovieInfo caribbeanMovie, String key){
        String title = caribbeanMovie.getName();
        String trailer = caribbeanMovie.getTrailer();
        String synopsis = caribbeanMovie.getSynopsis();
        String genre = caribbeanMovie.getGenre();
        String cover = caribbeanMovie.getCover();
        String director = caribbeanMovie.getDirector();
        String duration = caribbeanMovie.getDuration();
        String country = caribbeanMovie.getCountry();

        Movie mov = moviesMap.get(key);

        if (mov == null) {
            Movie movie = new Movie(title, trailer, synopsis, genre, cover, director, duration, country);
            movie.addAllCaribbeanSchedules(caribbeanMovie.getMovieSchedules(), caribbeanMovie.getFeatures());
            moviesMap.put(key, movie);

        } else {
            mov.addAllCaribbeanSchedules(caribbeanMovie.getMovieSchedules(), caribbeanMovie.getFeatures());
        }
    }

    private void putInMap(PalacioDelCineMovieInfo palacioMovie, String key){

        String title = palacioMovie.getTitle();
        String trailer = "";
        String synopsis = palacioMovie.getDescription();
        String genre = "";
        String cover = palacioMovie.getPoster();
        String director = "";
        String duration = "";
        String country = "";


        Movie mov = moviesMap.get(key);

        if (mov ==null) {
            Movie movie = new Movie(title, trailer, synopsis, genre, cover, director, duration, country);
            movie.addAllPalacioSchedules(palacioMovie.getMovieSchedules(), palacioMovie.getFeatures());
            moviesMap.put(key, movie);

        } else {
            mov.addAllPalacioSchedules(palacioMovie.getMovieSchedules(), palacioMovie.getFeatures());
        }

    }

    private String stripAndLowercase(String str) {
        if (str != null && StringUtils.isNotEmpty(str)) {
            return StringUtils.lowerCase(StringUtils.strip(str));
        }
        return "";
    }

    private String removeUnwantedChars(String str) {

        String result = str;
        if (str != null && StringUtils.isNotEmpty(str)) {
            for (String string : unWantedStrings) {
                result = StringUtils.remove(result, string);
            }
            result = stripAndLowercase(result);
            return  result;
        }
        return "";
    }

    public Map<String, Movie> getMoviesMap() {
        return moviesMap;
    }

}
