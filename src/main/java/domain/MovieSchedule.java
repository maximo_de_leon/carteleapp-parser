package domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maximodeleon on 11/13/16.
 */
public class MovieSchedule {

    List<MovieTheaterSchedule> caribbeanSchedule;
    List<MovieTheaterSchedule> palacioSchedule;

    public MovieSchedule() {
        this.caribbeanSchedule = new ArrayList<MovieTheaterSchedule>();
        this.palacioSchedule = new ArrayList<MovieTheaterSchedule>();
    }

    public List<MovieTheaterSchedule> getCaribbeanSchedule() {
        return caribbeanSchedule;
    }

    public void setCaribbeanSchedule(List<MovieTheaterSchedule> caribbeanSchedule) {
        this.caribbeanSchedule = caribbeanSchedule;
    }

    public List<MovieTheaterSchedule> getPalacioSchedule() {
        return palacioSchedule;
    }

    public void setPalacioSchedule(List<MovieTheaterSchedule> palacioSchedule) {
        this.palacioSchedule = palacioSchedule;
    }

    public void addCaribbeanSchedule(MovieTheaterSchedule schedule) {
        caribbeanSchedule.add(schedule);
    }

    public void addPalacioSchedule(MovieTheaterSchedule schedule) {
        palacioSchedule.add(schedule);
    }

}
