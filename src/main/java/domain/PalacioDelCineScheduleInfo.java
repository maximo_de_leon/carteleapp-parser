package domain;

import java.util.Set;

/**
 * Created by maximodeleon on 10/30/16.
 */
public class PalacioDelCineScheduleInfo {

    Set<String> times;
    PalacioDelCineTheaterInfo theater;

    public PalacioDelCineScheduleInfo(String theater, Set<String> times) {
        this.theater = new PalacioDelCineTheaterInfo(theater);
        this.times = times;
    }

    public Set<String> getTimes() {
        return times;
    }

    public void setTimes(Set<String> times) {
        this.times = times;
    }

    public PalacioDelCineTheaterInfo getTheater() {
        return theater;
    }

    public void setTheater(PalacioDelCineTheaterInfo place) {
        this.theater = place;
    }


    @Override
    public String toString() {
        return "PalacioDelCineScheduleInfo{" +
                "times=" + times +
                ", theater=" + theater +
                '}';
    }
}
