package domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maximodeleon on 10/30/16.
 */
public class CaribbeanCinemasMovieInfo {


    private String movieId;
    private String country;
    private String cover;
    private String director;
    private String duration;
    private String genre;
    private String language;
    private String name;
    private String rating;
    private String synopsis;
    private String trailer;
    private String writer;
    private String format;
    private List<CaribbeanCinermasScheduleInfo> schedules;
    MovieFeatures features;

    public CaribbeanCinemasMovieInfo(String movieId, String country, String cover, String director, String duration,
                                     String genre, String language, String name, String rating, String synopsis,
                                     String trailer, String writer) {
        this.movieId = movieId;
        this.country = country;
        this.cover = cover;
        this.director = director;
        this.duration = duration;
        this.genre = genre;
        this.language = language;
        this.name = name;
        this.rating = rating;
        this.synopsis = synopsis;
        this.trailer = trailer;
        this.writer = writer;
        setFeatures();
    }

    public String getMovieId() {
        return movieId;
    }

    public void setMovieId(String movieId) {
        this.movieId = movieId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public List<CaribbeanCinermasScheduleInfo> getSchedules() {
        return schedules;
    }

    public void setSchedules(List<CaribbeanCinermasScheduleInfo> schedules) {
        this.schedules = schedules;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    private void setFeatures () {
        MovieFeatures.Formato formato = parseFormat(language);
        MovieFeatures.Idioma idioma = parseLanguage() ;
        features = new MovieFeatures(idioma, formato);
    }

    public List<MovieTheaterSchedule> getMovieSchedules () {
        List<MovieTheaterSchedule> reSchedules = new ArrayList<MovieTheaterSchedule>();

        for (CaribbeanCinermasScheduleInfo scheduleInfo : schedules) {
            String schedule = scheduleInfo.getDetail();
            String theater = scheduleInfo.getTheater().getName();
            MovieTheaterSchedule theaterSchedule = new MovieTheaterSchedule(schedule, theater);
            reSchedules.add(theaterSchedule);
        }

        return reSchedules;
    }

    private MovieFeatures.Idioma parseLanguage() {
        if (language != null) {
            if (language.toLowerCase().contains("subtitulada")) {
                return MovieFeatures.Idioma.ING;
            } else if (language.toLowerCase().contains("espanol")) {
                return MovieFeatures.Idioma.ESP;
            }
        }

        return MovieFeatures.Idioma.ING;
    }

    private MovieFeatures.Formato parseFormat(String idioma) {
        if (idioma.toLowerCase().contains("4dx")) {
            return MovieFeatures.Formato._4DX;
        } else if (idioma.toLowerCase().contains("3d")) {
            return MovieFeatures.Formato._3D;
        } else if (idioma.toLowerCase().contains("cxc")) {
            return MovieFeatures.Formato.CXC;
        } else {
            return MovieFeatures.Formato._2D;
        }
    }

    public MovieFeatures getFeatures() {
        return features;
    }

    @Override
    public String toString() {
        return "CaribbeanCinemasMovieInfo{" +
                ", language='" + language + '\'' +
                ", name='" + name + '\'' +
                ", synopsis='" + synopsis + '\'' +
                ", format='" + format + '\'' +
                '}';
    }
}
