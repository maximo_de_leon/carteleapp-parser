package domain;

/**
 * Created by maximodeleon on 10/30/16.
 */
public class CaribbeanCinemasTheaterInfo {

    private String name;
    private CaribbeanCinemasRegionInfo region;
    private String address;
    private String phone;
    private String lat;
    private String lon;
    private String website;
    private String description;
    private String precio;
    private String id;

    public CaribbeanCinemasTheaterInfo(String name, CaribbeanCinemasRegionInfo region, String address, String phone,
                                       String lat, String lon, String website, String description,
                                       String precio, String id) {
        this.name = name;
        this.region = region;
        this.address = address;
        this.phone = phone;
        this.lat = lat;
        this.lon = lon;
        this.website = website;
        this.description = description;
        this.precio = precio;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CaribbeanCinemasRegionInfo getRegion() {
        return region;
    }

    public void setRegion(CaribbeanCinemasRegionInfo region) {
        this.region = region;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "CaribbeanCinemasTheaterInfo{" +
                "name='" + name + '\'' +
                ", region='" + region + '\'' +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                ", lat='" + lat + '\'' +
                ", lon='" + lon + '\'' +
                ", website='" + website + '\'' +
                ", description='" + description + '\'' +
                ", precio='" + precio + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}
