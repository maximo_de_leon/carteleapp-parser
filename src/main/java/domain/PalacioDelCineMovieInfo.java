package domain;

import org.jsoup.Jsoup;
import utils.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by maximodeleon on 10/30/16.
 */
public class PalacioDelCineMovieInfo {

    String title;
    String englishTitle;
    String description;
    String poster;
    String link;
    List<PalacioDelCineScheduleInfo> schedules;
    MovieFeatures features;

    public PalacioDelCineMovieInfo(String title, String description, String poster, String link) {
        this.title = title;
        this.description = description.substring(0, description.indexOf("<br/>"));
        this.poster = poster;
        this.link = link;
        setFeatures(title);
        parseEnglishTitle(link);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getLink() {
        return link;
    }

    public List<PalacioDelCineScheduleInfo> getSchedules() {
        return schedules;
    }

    public void setSchedules(List<PalacioDelCineScheduleInfo> schedules) {
        this.schedules = schedules;
    }

    public void AddSchedule(PalacioDelCineScheduleInfo schedule){
        if (this.schedules == null) {
            this.schedules = new ArrayList<PalacioDelCineScheduleInfo>();
            this.schedules.add(schedule);
        }
        else{
            this.schedules.add(schedule);
        }
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getEnglishTitle() {
        return englishTitle;
    }

    public void setEnglishTitle(String englishTitle) {
        this.englishTitle = englishTitle;
    }

    private void setFeatures (String title) {
        MovieFeatures.Formato formato  = parseFormat(title);
        MovieFeatures.Idioma idioma = parseLanguage(title);
        features = new MovieFeatures(idioma, formato);
    }

    public List<MovieTheaterSchedule> getMovieSchedules() {
        List<MovieTheaterSchedule> reSchedules = new ArrayList<MovieTheaterSchedule>();
        for (PalacioDelCineScheduleInfo scheduleInfo : schedules) {

            String schedule = "";
            String theater = scheduleInfo.getTheater().getName();
            for (String s : scheduleInfo.getTimes()) {
                schedule = schedule  + s + " ";
            }

            MovieTheaterSchedule theaterSchedule = new MovieTheaterSchedule(schedule, theater);
            reSchedules.add(theaterSchedule);
        }

        return reSchedules;
    }

    private MovieFeatures.Idioma parseLanguage(String desc) {
        if (desc.toLowerCase().contains("esp")) {
           return MovieFeatures.Idioma.ESP;
        } else if (!desc.toLowerCase().contains("esp")) {
            return MovieFeatures.Idioma.ING;
        } else {
            return MovieFeatures.Idioma.ESP;
        }
    }

    private MovieFeatures.Formato parseFormat(String desc) {
        if (desc.toLowerCase().contains("imax")) {
            return MovieFeatures.Formato.IMAX;
        } else if (desc.toLowerCase().contains("3d")) {
            return MovieFeatures.Formato._3D;
        } else if (desc.toLowerCase().contains("2d")) {
            return MovieFeatures.Formato._2D;
        } else {
            return MovieFeatures.Formato._2D;
        }
    }

    private void parseEnglishTitle(String link) {
        Logger logger = Logger.getInstance(PalacioDelCineMovieInfo.class);
        try {

            englishTitle = Jsoup.connect(link).userAgent("Mozilla")
                    .timeout(10000)
                    .get()
                    .select("#aspnetForm")
                    .select("#gralCont")
                    .select("#colLeft")
                    .select("h4")
                    .text();
        }catch (IOException e) {
            logger.logError("Error buscando el nombre en ingles para " + link, e);
        }
    }

    public MovieFeatures getFeatures() {
        return features;
    }

    @Override
    public String toString() {
        return "PalacioDelCineMovieInfo{" +
                "title='" + title + '\'' +
                "englishTitle='" + englishTitle + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

}
