package domain;

/**
 * Created by maximodeleon on 11/13/16.
 */
public class MovieTheaterSchedule {

    private String schedule;
    private String theaterName;

    public MovieTheaterSchedule(String schedule, String theaterName) {
        this.schedule = schedule;
        this.theaterName = theaterName;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getTheaterName() {
        return theaterName;
    }

    public void setTheaterName(String theaterName) {
        this.theaterName = theaterName;
    }


}
