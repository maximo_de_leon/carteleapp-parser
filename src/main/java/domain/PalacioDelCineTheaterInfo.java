package domain;

/**
 * Created by maximodeleon on 10/30/16.
 */
public class PalacioDelCineTheaterInfo {

    String name;


    public PalacioDelCineTheaterInfo(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "PalacioDelCineTheaterInfo{" +
                "name='" + name + '\'' +
                '}';
    }
}
