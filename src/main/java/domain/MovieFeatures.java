package domain;

/**
 * Created by maximodeleon on 11/12/16.
 */
public class MovieFeatures {

    enum Idioma {
        ESP("Espanol"),
        ING("Subtitulada");

        String value;

        Idioma(String val) {
            value = val;
        }
    };

    enum Formato {
        _3D,
        IMAX,
        CXC,
        _2D,
        _4DX
    };


    Idioma idioma;
    Formato formato;

    public Idioma getIdioma() {
        return idioma;
    }

    public void setIdioma(Idioma idioma) {
        this.idioma = idioma;
    }

    public Formato getFormato() {
        return formato;
    }

    public void setFormato(Formato formato) {
        this.formato = formato;
    }

    public MovieFeatures(Idioma idioma, Formato formato) {
        this.formato = formato;
        this.idioma = idioma;
    }

    @Override
    public int hashCode() {
        int idioma = this.idioma.value.length();
        int formato = this.formato.ordinal();

        return idioma * formato;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj != null) {
            if (obj instanceof MovieFeatures) {
                MovieFeatures feat = (MovieFeatures) obj;

                if (feat.getFormato().ordinal() == this.getFormato().ordinal()
                        && feat.getIdioma().value.equalsIgnoreCase(this.getIdioma().value)
                        ) {
                    return true;
                }

            } else {
                return false;
            }
        }
        return false;
    }
}
