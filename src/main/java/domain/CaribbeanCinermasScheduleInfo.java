package domain;

/**
 * Created by maximodeleon on 10/30/16.
 */
public class CaribbeanCinermasScheduleInfo {

    private CaribbeanCinemasTheaterInfo theater;
    private String detail;

    public CaribbeanCinermasScheduleInfo(CaribbeanCinemasTheaterInfo theater,  String detail) {
        this.theater = theater;
        this.detail = detail;
    }

    public CaribbeanCinemasTheaterInfo getTheater() {
        return theater;
    }

    public void setTheater(CaribbeanCinemasTheaterInfo theater) {
        this.theater = theater;
    }


    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Override
    public String toString() {
        return "CaribbeanCinermasScheduleInfo{" +
                "theater='" + theater + '\'' +
                ", detail='" + detail + '\'' +
                '}';
    }
}
