package domain;

/**
 * Created by maximodeleon on 10/30/16.
 */
public class CaribbeanCinemasRegionInfo {

    private String name;
    private String id;

    public CaribbeanCinemasRegionInfo(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "CaribbeanCinemasRegionInfo{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}
