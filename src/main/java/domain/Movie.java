package domain;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by maximodeleon on 11/13/16.
 */
public class Movie {

    private String name;
    private String trailer;
    private String synopsis;
    private String genre;
    private String cover;
    private String director;
    private String duration;
    private String country;
    private Map<MovieFeatures, MovieSchedule> scheduleMap;

    public Movie(String name, String trailer, String synopsis, String genre, String cover, String director, String duration, String country) {
        this.name = name;
        this.trailer = trailer;
        this.synopsis = synopsis;
        this.genre = genre;
        this.cover = cover;
        this.director = director;
        this.duration = duration;
        this.country = country;
        scheduleMap = new HashMap<MovieFeatures, MovieSchedule>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void addAllCaribbeanSchedules(List<MovieTheaterSchedule> schedules, MovieFeatures features){
        MovieSchedule movieSchedule = scheduleMap.get(features);

        if (movieSchedule != null) {
            for (MovieTheaterSchedule sche : schedules) {
                movieSchedule.addCaribbeanSchedule(sche);
            }
        } else {
            MovieSchedule schedule = new MovieSchedule();
            for (MovieTheaterSchedule movsche : schedules) {
                schedule.addCaribbeanSchedule(movsche);
            }

            scheduleMap.put(features, schedule);
        }
    }

    public void addAllPalacioSchedules(List<MovieTheaterSchedule> schedules, MovieFeatures features){
        MovieSchedule movieSchedule = scheduleMap.get(features);

        if (movieSchedule != null) {
            for (MovieTheaterSchedule sche : schedules) {
                movieSchedule.addPalacioSchedule(sche);
            }
        }
        else {
            MovieSchedule schedule = new MovieSchedule();
            for (MovieTheaterSchedule movsche : schedules) {
                schedule.addPalacioSchedule(movsche);
            }
            scheduleMap.put(features, schedule);
        }
    }

}

