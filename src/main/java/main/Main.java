package main;

import domain.CaribbeanCinemasMovieInfo;
import domain.PalacioDelCineMovieInfo;
import parsers.CaribbeanCinemasParser;
import parsers.PalacioDelCineParser;
import utils.Logger;
import utils.MoviesMatcher;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by maximodeleon on 10/30/16.
 */
public class Main {

    public static void main(String [] args) {

        Logger logger = Logger.getInstance(Main.class);

        //TODO organizar mejor los parseadores para que tengan un metodo
        // TODO que parsee un solo elemento.
        Date start = Calendar.getInstance().getTime();
        logger.logInfo("Se van a empezar a procesar los pelicular de palacio del cine");
        List<PalacioDelCineMovieInfo> moviesPalacio = new PalacioDelCineParser().parse();
        logger.logInfo("Se van a empezar a procesar los pelicular de caribbean cinemas");
        List<CaribbeanCinemasMovieInfo> moviesCaribbean = new CaribbeanCinemasParser().parse();

        MoviesMatcher matcher = new MoviesMatcher(moviesPalacio, moviesCaribbean);
        matcher.match();
        Date end = Calendar.getInstance().getTime();
        logger.logInfo("Exito");
        logger.logElapsedTime(start, end);
    }

}
