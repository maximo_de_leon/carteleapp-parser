package parsers;

import domain.CaribbeanCinemasMovieInfo;
import domain.CaribbeanCinemasRegionInfo;
import domain.CaribbeanCinemasTheaterInfo;
import domain.CaribbeanCinermasScheduleInfo;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import utils.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by maximodeleon on 10/30/16.
 */
public class CaribbeanCinemasParser implements IParser{

    private final String URL_STRING = "http://caribbeancinemasrd.com/wp-content/themes/caribbeancinemas/xml/feddxml.xml";
    private Document doc;

    /* Movies */
    private final String MOVIE_PATH = "/CaribbeanCinemas/Movies/Movie";
    private final String MOVIE_ID_ATTRIBUTE = "id";
    private final String COUNTRY_TAG = "Country";
    private final String COVER_TAG = "Cover";
    private final String DIRECTOR_TAG = "Director";
    private final String DURATION_TAG = "Duration";
    private final String GENRE_TAG = "Genres";
    private final String LANGUAGE_TAG = "Language";
    private final String NAME_TAG = "Name";
    private final String RATING_TAG = "Rating";
    private final String SYNOPSIS_TAG = "Synopsis";
    private final String TRAILER_TAG = "TrailerURL";
    private final String WRITTER_TAG = "Writers";
    /* Schedules */
    private final String SCHEDULE_PATH = "/CaribbeanCinemas/Schedules/Schedule";
    private final String MOVIE_TAG = "Movie";
    /* Theaters */
    private final String THEATER_PATH = "/CaribbeanCinemas/Theaters/Theater";
    private final String NAME_THEATER_TAG = "Name";
    private final String REGION_TAG = "Region";
    private final String ADDRESS_TAG = "Address";
    private final String PHONE_TAG = "Phone";
    private final String LAT_TAG = "lat";
    private final String LON_TAG = "lon";
    private final String WEBSITE_TAG = "website";
    private final String DESC_TAG = "desc";
    private final String PRECIO_TAG = "precio";
    /* Region */
    private final String REGION_PATH = "/CaribbeanCinemas/Regions/Region";
    private final String REGION_NAME_TAG = "Name";
    private final String REGION_ID_ATTRIBUTE = "id";

    Logger  logger = Logger.getInstance(CaribbeanCinemasParser.class);

    public CaribbeanCinemasParser() {
        try {
            logger.logInfo("Inicializando clase");
            URL url = new URL(URL_STRING);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            doc = db.parse(url.openStream());
        }
        catch (MalformedURLException e) {
            logger.logError("Se produjo un error con la url", e);
        }
        catch (ParserConfigurationException e) {
            logger.logError("Se produjo un error de parseo", e);
        }
        catch (IOException e ){
            logger.logError("Se produjo un error de IO", e);
        }
        catch (SAXException e) {
            logger.logError("Se produjo un error de SAX", e);
        }
    }

    public List<CaribbeanCinemasMovieInfo> parse() {
          XPath xPath =  XPathFactory.newInstance().newXPath();
          return parseMovies(xPath);
    }

    private CaribbeanCinemasRegionInfo parseRegion(XPath xPath, String regionId) {

        CaribbeanCinemasRegionInfo region = null;

        try {
            NodeList nodeList = (NodeList) xPath.compile(REGION_PATH +"[@id=" + regionId + "]").evaluate(doc, XPathConstants.NODESET);
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node nNode = nodeList.item(i);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    String name = eElement.getElementsByTagName(REGION_NAME_TAG)
                            .item(0)
                            .getTextContent();
                    String id = eElement.getAttribute(REGION_ID_ATTRIBUTE);

                    region = new CaribbeanCinemasRegionInfo(name, id);
                }
            }
        }
        catch (XPathExpressionException e) {
            logger.logError("Error parseando la informacion de la region", e);
        }

        return region;
    }

    private CaribbeanCinemasTheaterInfo parseTheater(XPath xPath, String theaterId) {

        CaribbeanCinemasTheaterInfo theater = null;
        try {
            NodeList nodeList = (NodeList) xPath.compile(THEATER_PATH + "[@id=" + theaterId+"]")
                                                    .evaluate(doc, XPathConstants.NODESET);
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node nNode = nodeList.item(i);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    String name = eElement.getElementsByTagName(NAME_THEATER_TAG)
                            .item(0)
                            .getTextContent();
                    String regionId = eElement.getElementsByTagName(REGION_TAG)
                            .item(0)
                            .getTextContent();
                    String address = eElement.getElementsByTagName(ADDRESS_TAG)
                            .item(0)
                            .getTextContent();
                    String phone = eElement.getElementsByTagName(PHONE_TAG)
                            .item(0)
                            .getTextContent();
                    String lat = eElement.getElementsByTagName(LAT_TAG)
                            .item(0)
                            .getTextContent();
                    String lon = eElement.getElementsByTagName(LON_TAG)
                            .item(0)
                            .getTextContent();
                    String website = eElement.getElementsByTagName(WEBSITE_TAG)
                            .item(0)
                            .getTextContent();
                    String desc = eElement.getElementsByTagName(DESC_TAG)
                            .item(0)
                            .getTextContent();
                    String precio = eElement.getElementsByTagName(PRECIO_TAG)
                            .item(0)
                            .getTextContent();

                    CaribbeanCinemasRegionInfo region = parseRegion(xPath, regionId);

                    theater =
                            new CaribbeanCinemasTheaterInfo(name, region, address, phone, lat, lon, website, desc,
                                    precio, theaterId);
                }
            }
        }
        catch (XPathExpressionException e) {
            logger.logError("Se produjo un error parseando la informacion del cine", e);
        }
        return theater;
    }

    private List<CaribbeanCinermasScheduleInfo> parseSchedules(XPath xPath, String movieId) {

        List<CaribbeanCinermasScheduleInfo> schedules = new ArrayList<CaribbeanCinermasScheduleInfo>();
        try {
            NodeList nodeList = (NodeList) xPath.compile(SCHEDULE_PATH + "/" + MOVIE_TAG + " [text() = " + movieId + "]")
                    .evaluate(doc, XPathConstants.NODESET);
            for (int i = 0; i < nodeList.getLength(); i++) {
                //Theater
                String nTheater = nodeList.item(i).getPreviousSibling().getFirstChild().getNodeValue();
                //Detail
                String nDetail = nodeList.item(i).getNextSibling().getFirstChild().getNodeValue();

                CaribbeanCinemasTheaterInfo theaterInfo =  parseTheater(xPath, nTheater);

                schedules.add(new CaribbeanCinermasScheduleInfo(theaterInfo, nDetail));

            }
        }
        catch (XPathExpressionException e) {
            logger.logError("Se produjo un error parseando el horario", e);
        }

        return schedules;
    }

    private List<CaribbeanCinemasMovieInfo>  parseMovies(XPath xPath) {
        List<CaribbeanCinemasMovieInfo> movies = new ArrayList<CaribbeanCinemasMovieInfo>();
        try {
            NodeList nodeList = (NodeList) xPath.compile(MOVIE_PATH).evaluate(doc, XPathConstants.NODESET);

            int size = nodeList.getLength();
            logger.logInfo("Se van a empezar a parsear las peliculas (" + size + ")");
            for (int i = 0; i < size; i++) {
                Node nNode = nodeList.item(i);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;

                    String id =eElement.getAttribute(MOVIE_ID_ATTRIBUTE);
                    String name = eElement.getElementsByTagName(NAME_TAG)
                            .item(0)
                            .getTextContent();
                    String country = eElement.getElementsByTagName(COUNTRY_TAG)
                            .item(0)
                            .getTextContent();
                    String cover = eElement.getElementsByTagName(COVER_TAG)
                            .item(0)
                            .getTextContent();
                    String director = eElement.getElementsByTagName(DIRECTOR_TAG)
                            .item(0)
                            .getTextContent();
                    String duration = eElement.getElementsByTagName(DURATION_TAG)
                            .item(0)
                            .getTextContent();
                    String genre = eElement.getElementsByTagName(GENRE_TAG)
                            .item(0)
                            .getTextContent();
                    String language = eElement.getElementsByTagName(LANGUAGE_TAG)
                            .item(0)
                            .getTextContent();
                    String rating = eElement.getElementsByTagName(RATING_TAG)
                            .item(0)
                            .getTextContent();
                    String sypnosis = eElement.getElementsByTagName(SYNOPSIS_TAG)
                            .item(0)
                            .getTextContent();
                    String trailer = eElement.getElementsByTagName(TRAILER_TAG)
                            .item(0)
                            .getTextContent();
                    String writter = eElement.getElementsByTagName(WRITTER_TAG)
                            .item(0)
                            .getTextContent();

                    CaribbeanCinemasMovieInfo movie =
                            new CaribbeanCinemasMovieInfo(id, country, cover, director, duration, genre, language, name
                                    ,rating, sypnosis, trailer, writter);

                    List<CaribbeanCinermasScheduleInfo> scheduleInfos = parseSchedules(xPath, id);
                    movie.setSchedules(scheduleInfos);
                    movies.add(movie);

                    logger.logInfo("Parseada " + (i + 1) + " de " + size + ": " +  name);
                }
            }
        }
        catch (XPathExpressionException e) {
            logger.logError("Se produjo un error parseando la informacion de la pelicula", e);
        }
        return movies;
    }

}
