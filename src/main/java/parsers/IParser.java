package parsers;

import java.util.List;

/**
 * Created by maximodeleon on 10/30/16.
 */
public interface IParser {
    public List<?> parse();
}
