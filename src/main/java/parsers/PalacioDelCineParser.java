package parsers;

import domain.PalacioDelCineMovieInfo;
import domain.PalacioDelCineScheduleInfo;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import utils.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by maximodeleon on 10/30/16.
 */
public class PalacioDelCineParser implements IParser{

    private final String URL_STRING = "http://www.palaciodelcine.com.do/info/rss/rss.ashx";
    private Document doc;

    private final String MOVIE_PATH = "/rss/channel/item";
    private final String TITLE_TAG = "title";
    private final String POSTER_TAG = "enclosure";
    private final String POSTER_ATTRIBUTE = "url";
    private final String DESCRIPTION_TAG = "description";
    private final String LINK_TAG = "link";

    private final String TIMES_PATTERN = "\\d{2}:\\d{2}";
    private final String NAME_PATTERN = "[a-zA-Z\\s]+";

    Logger logger = Logger.getInstance(PalacioDelCineParser.class);

    public PalacioDelCineParser() {
        try {
            logger.logInfo("Inicializando clase");
            URL url = new URL(URL_STRING);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            doc = db.parse(url.openStream());
        }
        catch (MalformedURLException e ) {
            logger.logError("Se produjo un error con la url", e);
        }
        catch (ParserConfigurationException e) {
            logger.logError("Se produjo un error de parseo", e);
        }
        catch (IOException e ){
            logger.logError("Se produjo un error de IO", e);
        }
        catch (SAXException e) {
            logger.logError("Se produjo un error de SAX", e);
        }

    }

    public List<PalacioDelCineMovieInfo> parse(){
        List<PalacioDelCineMovieInfo> movies = new ArrayList<PalacioDelCineMovieInfo>();
        try {
            XPath xPath = XPathFactory.newInstance().newXPath();
            NodeList nodeList = (NodeList) xPath.compile(MOVIE_PATH).evaluate(doc, XPathConstants.NODESET);
            int size = nodeList.getLength();
            logger.logInfo("Se van a empezar a parsear las peliculas (" + size + ")");
            for (int i = 0; i < size; i++) {
                Node nNode = nodeList.item(i);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    String title =  eElement
                            .getElementsByTagName(TITLE_TAG)
                            .item(0)
                            .getTextContent();
                    String poster = eElement
                            .getElementsByTagName(POSTER_TAG)
                            .item(0)
                            .getAttributes().getNamedItem(POSTER_ATTRIBUTE).getNodeValue();
                    String description = eElement
                            .getElementsByTagName(DESCRIPTION_TAG)
                            .item(0)
                            .getTextContent();
                    String link = eElement
                                    .getElementsByTagName(LINK_TAG)
                                    .item(0)
                                    .getTextContent();

                    PalacioDelCineMovieInfo movie = new PalacioDelCineMovieInfo(title, description, poster, link);
                    parseTheaterAndTimes(description, movie);
                    movies.add(movie);

                    logger.logInfo("Parseada " + (i + 1) + " de " + size + ": " +  title);
                }
            }
        }
        catch (XPathExpressionException e) {
        }

        return movies;

    }

    private void parseTheaterAndTimes(String desc, PalacioDelCineMovieInfo movie) {

        String sub = StringUtils.stripAccents(
                desc.substring(desc.indexOf("Horarios") + "Horarios <br/>".length() + 1)).replaceAll("\\t+", "");

        String  [] splitted = sub.split("<br/>");

        for (String ss : splitted) {
            Pattern pattern =  Pattern.compile(NAME_PATTERN);
            Matcher matcher = pattern.matcher(ss);
            while (matcher.find()) {
                String group = matcher.group();
                if (group != null && group.compareTo("") != 0 && group.compareTo(" ") != 0) {
                    Pattern pattern2 = Pattern.compile(TIMES_PATTERN);
                    Matcher matcher2 = pattern2.matcher(ss);
                    Set<String> times = new HashSet<String>();
                    while (matcher2.find()) {
                        times.add(matcher2.group());
                    }
                    PalacioDelCineScheduleInfo info = new PalacioDelCineScheduleInfo(group, times);
                    movie.AddSchedule(info);
                }

            }
        }
    }

}
